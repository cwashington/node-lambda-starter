Run the following commands:


Install: 

```bash
npm install
```

Serve (run application):

```bash
npm run serve
```

Now you can go to your browser and see the output:

http://localhost:3000/users/cwashington?limit=3&offset=5&someOther=var