import awsLambdaFastify from 'aws-lambda-fastify';
import { app } from './src/app';

const handler = awsLambdaFastify(app);

exports = {
    handler
}