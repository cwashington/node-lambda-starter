import {fastify, FastifyRequest, FastifyReply} from 'fastify';
import fastifyHelmet from 'fastify-helmet';

export const app = fastify({
    logger: true
});

app.register(fastifyHelmet);

app.register(require('./routes/user'));


if (require.main === module) {
    async function start(port: number, host: string) {
        await app.listen(port, host);
    }
    
    
    start(3000, '0.0.0.0');
}

exports = app;
