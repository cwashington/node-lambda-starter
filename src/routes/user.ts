import { FastifyInstance, FastifyRequest, FastifyReply } from "fastify";
import config from 'config';

import { someGenericService } from "../services/some-generic-services";



export default function(fastify: FastifyInstance, _: unknown, done: any) {
    fastify.get('/users/:userId', (request: FastifyRequest, reply: FastifyReply) => {
        reply.code(200).send(someGenericService.getInfo(request));
    });

    done();
}