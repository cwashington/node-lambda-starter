import config from 'config';

export class SomeGenericServices {

    private name: string;

    setName (name: string): void {
        this.name = name;
    }
    getName(): string {
        return this.name;
    }
    constructor(name: string) {
        this.name = name;
    }

    getInfo(req: any) {
        const {query, params} = <any>req;

        return {
            ...query,
            ...params,
            greeting: config.get('greeting'),
            chris: config.get('chris'),
            version: 1,
            name: someGenericService.getName()
        }
    }


}


export const someGenericService = new SomeGenericServices('my name');

