import nodeConfig from 'config';

export const config = {
    get(key: string, defaultValue?: any) {
        return nodeConfig.has('key') ? nodeConfig.get(key) : defaultValue;
    },
    has: nodeConfig.has,
    util: nodeConfig.util
}